using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using smsexample.Data;
using smsexample.Data.Models;

namespace smsexample.Pages
{
    public class SpamModel : PageModel
    {
        public bool HasPosted = false;
        public bool IsSpam = false;

        public void OnGet()
        {
            return;
        }


        [BindProperty]
        public Message Message { get; set; }



        public void OnPost()
        {
            if (!ModelState.IsValid)
            {
                return;
            }
            var prediction = Predictions.PredictionHelper.TrainedModel.Predict(new SentimentData()
            {
                Sentiment = 0,
                SentimentText = Message.Text
            });
            Console.WriteLine(@"Recieved message {0} for prediction.", Message.Text);
            Console.WriteLine(@"Prediction is: {0}.", prediction.Sentiment ? "Spam." : "No spam");
            IsSpam = prediction.Sentiment;
            HasPosted = true;
            return;
        }
    }
}