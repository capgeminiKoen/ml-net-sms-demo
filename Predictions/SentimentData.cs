﻿using Microsoft.ML.Runtime.Api;

namespace smsexample
{
    public class SentimentData
    {
        [Column("1")]
        public string SentimentText;

        [Column("0", name: "Label")]
        public float Sentiment;
    }
}
