using System.ComponentModel.DataAnnotations;

namespace smsexample.Data.Models
{
    public class Message
    {
        [Required()]
        public string Text { get; set; }
    }
}