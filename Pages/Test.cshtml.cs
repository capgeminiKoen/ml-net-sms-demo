using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using smsexample.Data;
using smsexample.Data.Models;

namespace smsexample.Pages
{
    public class TestModel : PageModel
    {

        private readonly AppDbContext _db;

        public TestModel(AppDbContext db)
        {
            _db = db;
        }


        public IList<Customer> Customers { get; private set; }

        public async Task OnGetAsync()
        {
            Customers = await _db.Customers.AsNoTracking().ToListAsync();
        }


        [BindProperty]
        public Customer Customer { get; set; }


        public async Task<IActionResult> OnPostAsync()
        {
            Customers = await _db.Customers.AsNoTracking().ToListAsync();
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _db.Customers.Add(Customer);
            await _db.SaveChangesAsync();
            Customers = await _db.Customers.AsNoTracking().ToListAsync();
            return Page();
        }
    }
}