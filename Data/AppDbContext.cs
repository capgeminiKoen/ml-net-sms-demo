
using Microsoft.EntityFrameworkCore;
using smsexample.Data.Models;

namespace smsexample.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}