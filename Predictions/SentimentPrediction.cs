﻿using Microsoft.ML.Runtime.Api;

namespace smsexample
{
    public class SentimentPrediction
    {
        [ColumnName("PredictedLabel")]
        public bool Sentiment;
    }
}